﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdministrarComercio.aspx.cs" Inherits="PymeSearch.Views.AdministrarComercio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!-- Basic Page Needs
================================================== -->
    <title>Listeo</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- CSS
================================================== -->
    <script src="../Assets/templateStyle/scripts/jquery-3.5.1.min.js"></script>
    <link href="../Assets/templateStyle/css/style.css" rel="stylesheet" />
    <link href="../Assets/templateStyle/css/main-color.css" rel="stylesheet" />
    <script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
    <script src="../Assets/Js/AdministrarComercio.js"></script>
    <link href="../Assets/templateStyle/Principal.css" rel="stylesheet" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Assets/templateStyle/sweetalert.min.js"></script>
</head>

<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Header Container
================================================== -->
        <header id="header-container">

            <!-- Header -->
            <div id="header">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo" style="height: 50px; width: 100px;">
                            <a href="Principal.aspx"></a>
                            <img src="../images/LogoPymeSearchTransparent.png" />
                            <%--<h5>PymeSearch</h5>--%>
                        </div>

                        <!-- Main Navigation -->
                        <nav id="navigation" class="style-1">
                            
                            <ul id="responsive">

                                <li><a class="current" href="AdministrarComercio.aspx">Administrar Comercio</a></li>
                                <li><a class="current" href="#">Agregar</a>
                                    <ul>
                                         <li><a href="Promocion.aspx">Promoción</a></li>
                                        <li><a href="Item.aspx">Servicio/Articulo</a></li>
                                    </ul>
                                </li>                               
                            </ul>
                        </nav>
                    </div>
                    <div class="right-side">
                        <div class="header-widget">
                            <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i>Ingresar/Registrar comercio</a>
                        </div>
                    </div>
                    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

                        <div class="small-dialog-header">
                            <h3>Ingresar</h3>
                        </div>
                        <div class="sign-in-form style-1">
                        <div class="tabs-container alt">

                                <div class="tab-content" id="tab1" style="display: none;">
                                    <form method="post" class="login" action="ListadoOrdenes.aspx">

                                        <p class="form-row form-row-wide">
                                            <label for="username">
                                                Usuario:
										<i class="im im-icon-Male"></i>
                                                <input type="text" class="input-text" name="username" id="username" required="required" />
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password">
                                                Contraseña:
										<i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password" id="password" required="required" />
                                            </label>
                                            <span class="lost_password">
                                                <a href="#">Olvidó su contraseña?</a>
                                            </span>
                                        </p>

                                        <div class="form-row">
                                            <input type="submit" class="button border margin-top-5" name="login" value="Iniciar sesión" onclick="APP.Principal.iniciarSesion()" />
                                            <input type="submit" class="button border margin-top-5" name="login" value="Registrarse" />
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="clearfix"></div>
        <!-- Banner
================================================== -->
        <div id="titlebar" class="gradient">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h2><i class="sl sl-icon-plus"></i>Registrar Comercio</h2>

                        <!-- Breadcrumbs -->
                        <nav id="breadcrumbs">
                            <ul>
                                <li><a href="ListadoOrdenes.aspx">Principal</a></li>
                                <li>Registrar Comercio</li>
                            </ul>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
        <!-- Content
================================================== -->
        <!-- Container -->
        <div class="container">
           <form>
            <div class="row">
                <div class="col-lg-12">

                    <div class="notification notice large margin-bottom-55">
                        <h4>No tienes una cuenta? 🙂</h4>
                        <p>Crea una cuenta totalmente gratis para que tú comercio sea visible para todos.</p>
                    </div>

                      <div id="add-listing" class="separated-form">

                        <div class="add-listing-section">

                                <div class="add-listing-headline">
                                <%--<h3><i class="sl sl-icon-doc"></i>Información Principal</h3>--%>
                                <h3>Información Principal</h3>
                            </div>
                                <div class="row with-forms">
                                    <div class="col-md-6">
                                        <h5>Nombre <i data-tip-content="Nombre del comercio"></i></h5>
                                        <input class="search-field" id="in_nombre" type="text"  oninvalid="this.setCustomValidity('Por favor ingresar Nombre')" required=""  oninput="setCustomValidity('')"/>
                                    </div>

                                    <div class="col-md-6">
                                        <h5>Teléfono</h5>
                                        <input class="search-field" id="in_telefono" type="text" required="required" oninvalid="this.setCustomValidity('Por favor ingresar el teléfono')" oninput="setCustomValidity('')"/>
                                    </div>
                                </div>
                                <div class="row with-forms">
                                    <div class="col-md-6">
                                        <h5>Correo</h5>
                                        <input class="search-field" id="in_correo" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"  required="required" oninvalid="this.setCustomValidity('Por favor ingresar correo correctamente: @')" oninput="setCustomValidity('')"/>
                                    </div>

                                    <div class="col-md-6">
                                       <h5>Tipo Comercio</h5>
                                        <select class="chosen-select-no-single" id="s_tipoComercio" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="row with-forms">
                                    <div class="col-md-6">
                                        <h5 class="fb-input"><i class="fa fa-facebook-square"></i>Facebook <span>(optional)</span></h5>
                                        <input type="text" placeholder="https://www.facebook.com/" id="in_facebook"/>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="twitter-input"><i class="fa fa-instagram"></i>Instagram <span>(optional)</span></h5>
                                        <input type="text" placeholder="https://www.instagram.com/" id="in_instagram"/>
                                    </div>
                                </div>
                        </div>
                                <div class="add-listing-section margin-top-45">

                                    <!-- Headline -->
                                    <div class="add-listing-headline">
                                        <h3><i class="sl sl-icon-location"></i><a data-toggle="collapse" href="#collapse2">Dirección</a></h3>
                                    </div>

                                    <%--<div class="submit-section">--%>

                                    <!-- Row -->
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="row with-forms">

                                            <div class="row with-forms">
                                                <div class="col-md-12">
                                                    <h5>Dirección Detallada</h5>
                                                    <input class="search-field col-md-12" type="text" value="" required="required"  id="in_direccion" oninvalid="this.setCustomValidity('Por favor ingresar la dirección')"  oninput="setCustomValidity('')" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row with-forms">
                                            <div id="MapaAdministracionComercio">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="add-listing-section margin-top-45">

                                    <!-- Headline -->
                                    <div class="add-listing-headline">
                                        <h3><i class="sl sl-icon-picture""></i><a data-toggle="collapse" href="#collapse3">Fotos del Comercio</a></h3>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <!-- Dropzone -->
                                        <div class="submit-section">
                                           <%-- <form action="/file-upload" class="dropzone"></form>--%>
                                            <input type="file" name="UploadFile" id="txtUploadFile" class="dropzone" multiple />
                                        </div>
                                    </div>
                                </div>
                                <div class="add-listing-section margin-top-45">

                                    <div class="add-listing-headline">
                                        <h3><i class="sl sl-icon-clock"></i><a data-toggle="collapse" href="#collapse4">Horario de atención</a></h3>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="submit-section">
                                            <!-- Lunes -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Lunes</h5>
                                                </div>
                                                <div class="col-md-5">
                                                   <select class="chosen-select" data-placeholder="Apertura" id="s_lunesApertura">
                                               
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_lunesCierre">
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Martes -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Martes</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_martesAperturas">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_martesCierre">
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Miércoles-->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Miércoles</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_miercolesApertura">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_miercolesCierre">
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Day -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Jueves</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_juevesApertura">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_juevesCierre">
                                                    </select>
                                                </div>
                                            </div>
                                    
                                            <!-- jueves -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>viernes</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_viernesApertura">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_viernesCierre">
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Jueves -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Sábada</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_sabadoApertura">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_sabadoCierre">
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- Viernes -->
                                            <div class="row opening-day js-demo-hours">
                                                <div class="col-md-2">
                                                    <h5>Domingo</h5>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Apertura" id="s_domingoApertura">
                                                    </select>
                                                </div>
                                                <div class="col-md-5">
                                                    <select class="chosen-select" data-placeholder="Cierre" id="s_domingoCierre">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                       <%-- <a href="#" type="submit" class="button preview">Guardar <i class="fa fa-save"></i></a>--%>
                        <input type="submit" class="button border fw margin-top-10" name="register" value="Guardar" />
                    </div>
                </div>
            </div>
           </form>
       </div>
       


                <!-- Footer
================================================== -->
                <div class="copyrights"><h3>© 2019 Listeo. All Rights Reserved.</h3></div>
                <div id="backtotop"><a href="#"></a></div>


    </div>
    <!-- Scripts
================================================== -->

    <script src="../Assets/templateStyle/scripts/jquery-migrate-3.3.1.min.js"></script>
    <script src="../Assets/templateStyle/scripts/mmenu.min.js"></script>
    <script src="../Assets/templateStyle/scripts/chosen.min.js"></script>
    <script src="../Assets/templateStyle/scripts/slick.min.js"></script>
    <script src="../Assets/templateStyle/scripts/rangeslider.min.js"></script>
    <script src="../Assets/templateStyle/scripts/magnific-popup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/waypoints.min.js"></script>
    <script src="../Assets/templateStyle/scripts/counterup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/jquery-ui.min.js"></script>
    <script src="../Assets/templateStyle/scripts/tooltips.min.js"></script>
    <script src="../Assets/templateStyle/scripts/custom.js"></script>

    <!-- Opening hours added via JS (this is only for demo purpose) -->
    <script>
        $(".opening-day.js-demo-hours .chosen-select").each(function () {
            $(this).append('' +
                '<option></option>' +
                '<option>Cierre</option>' +
                '<option>1 AM</option>' +
                '<option>2 AM</option>' +
                '<option>3 AM</option>' +
                '<option>4 AM</option>' +
                '<option>5 AM</option>' +
                '<option>6 AM</option>' +
                '<option>7 AM</option>' +
                '<option>8 AM</option>' +
                '<option>9 AM</option>' +
                '<option>10 AM</option>' +
                '<option>11 AM</option>' +
                '<option>12 AM</option>' +
                '<option>1 PM</option>' +
                '<option>2 PM</option>' +
                '<option>3 PM</option>' +
                '<option>4 PM</option>' +
                '<option>5 PM</option>' +
                '<option>6 PM</option>' +
                '<option>7 PM</option>' +
                '<option>8 PM</option>' +
                '<option>9 PM</option>' +
                '<option>10 PM</option>' +
                '<option>11 PM</option>' +
                '<option>12 PM</option>');
        });


    </script>
    <script type="text/javascript">
        function GetUsuario(){

            var username = '<%=Session["Usuario"]%>';
            return username;
        }
    </script>

    <!-- DropZone | Documentation: http://dropzonejs.com -->
    <script src="../Assets/templateStyle/scripts/dropzone.js"></script>
    <!-- Style Switcher
================================================== -->
    <div id="style-switcher">
        <h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>

        <div>
            <ul class="colors" id="color1">
                <li><a href="#" class="main" title="Main"></a></li>
                <li><a href="#" class="blue" title="Blue"></a></li>
                <li><a href="#" class="green" title="Green"></a></li>
                <li><a href="#" class="orange" title="Orange"></a></li>
                <li><a href="#" class="navy" title="Navy"></a></li>
                <li><a href="#" class="yellow" title="Yellow"></a></li>
                <li><a href="#" class="peach" title="Peach"></a></li>
                <li><a href="#" class="beige" title="Beige"></a></li>
                <li><a href="#" class="purple" title="Purple"></a></li>
                <li><a href="#" class="celadon" title="Celadon"></a></li>
                <li><a href="#" class="red" title="Red"></a></li>
                <li><a href="#" class="brown" title="Brown"></a></li>
                <li><a href="#" class="cherry" title="Cherry"></a></li>
                <li><a href="#" class="cyan" title="Cyan"></a></li>
                <li><a href="#" class="gray" title="Gray"></a></li>
                <li><a href="#" class="olive" title="Olive"></a></li>
            </ul>
        </div>

    </div>
</body>
</html>

