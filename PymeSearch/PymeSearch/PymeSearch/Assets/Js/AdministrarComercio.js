﻿var APP = window.APP || {}

APP.AdministrarComercio = function () {
    
    var nombre = $("#in_nombre").val();
    var telefono = $("#in_telefono").val();
    var correo = $("#in_correo").val();
    var tipoComercio = $("#s_tipoComercio").val();
    var facebook = $("#in_facebook").val();
    var instagram = $("#in_instagram").val();
    var direccion = $("#in_direccion").val();
    //var fotos = $("#").val();
    var lunesApertura = $("#s_lunesApertura").val();
    var lunesCierre = $("#s_lunesCierre").val();
    var martesApertura = $("#s_martesAperturas").val();
    var martescierre = $("#s_martesCierre").val();
    var miercolesApertura = $("#s_miercolesApertura").val();
    var miercolesCierre = $("#s_miercolesCierre").val();
    var juevesApertura = $("#s_juevesApertura").val();
    var juevesCierre = $("#s_juevesCierre").val();
    var viernesApertura = $("#s_viernesApertura").val();
    var viernesCierre = $("#s_viernesCierre").val();
    var sabadoApertura = $("#s_sabadoApertura").val();
    var sabadoCierre = $("#s_sabadoCierre").val();
    var domingoApertura = $("#s_domingoApertura").val();
    var domingoCierre = $("#s_domingoCierre").val();
    var lon;
    var lat;
    var MapaAdministracionComercio;
    var init = function () {
        console.log('AdministrarComercio');
        inicializarComponentes();
    }

    var inicializarCoordenadas = function (lat, lon) {
        MapaAdministracionComercio = L.map('MapaAdministracionComercio', { center: [lat, lon], zoom: 15, maxZoom: 18, minZoom: 3,zoomControl:false })
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
        }).addTo(MapaAdministracionComercio);
        L.control.scale().addTo(MapaAdministracionComercio);
        var UbicacionActual = L.marker([lat, lon], { draggable: true }).addTo(MapaAdministracionComercio);
        UbicacionActual.bindPopup("<b>Tu comercio se encuentra aquí</b><br>");
    }
    var inicializarComponentes = function () {
        obtenerGeolocalizacion();
    }
    var obtenerGeolocalizacion = function () {


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (objPosition) {
               lon = objPosition.coords.longitude;
               lat = objPosition.coords.latitude;
                inicializarCoordenadas(lat, lon);
                //content.innerHTML = "<p><strong>Latitud:</strong> " + lat + "</p><p><strong>Longitud:</strong> " + lon + "</p>";

            }, function (objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        //content.innerHTML = "No se ha permitido el acceso a la posición del usuario.";
                        break;
                    case objPositionError.POSITION_UNAVAILABLE:
                        // content.innerHTML = "No se ha podido acceder a la información de su posición.";
                        break;
                    case objPositionError.TIMEOUT:
                        //content.innerHTML = "El servicio ha tardado demasiado tiempo en responder.";
                        break;
                    default:
                    //content.innerHTML = "Error desconocido.";
                }
            }, {
                maximumAge: 75000,
                timeout: 15000
            });
        }
        else {
            //content.innerHTML = "Su navegador no soporta la API de geolocalización.";
        }
    }
    var guardarcomercio = function () {
        var usuario = GetUsuario();
        var comercio = {
            nombreComercio: nombre,
            telefono: telefono,
            correo: correo,
            tipoComercio: tipoComercio,
            facebook: facebook,
            instagram: instagram,
            direccion: direccion,
            latitud: lat,
            longitud: lon,
            usuario: usuario
        }

        $.ajax({
            type: "POST",
            url: "../Views/AdministrarComercio.aspx/CrearComercio",
            dataType: "json",
            data: JSON.stringify({

                comercio:comercio

            }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                swal("Registro guardado correctamente", "", "success");
            },
            failure: function (response) {
                swal("Error al guardar el registro", "", "error");
            },
            error: function (response) {
                swal("Error al guardar el registro", "", "error");
            }
        });
    }
    var limpiar = function () {

    }
    return {
        init: init,
        guardarcomercio: guardarcomercio
    }
}();
$(document).ready(function () {
    APP.AdministrarComercio.init();
});