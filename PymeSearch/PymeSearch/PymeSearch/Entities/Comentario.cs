﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Comentario
    {
        public int Id { get; set; }
        public string comentario { get; set; }
        public DateTime fechaComentario { get; set; }
    }
}