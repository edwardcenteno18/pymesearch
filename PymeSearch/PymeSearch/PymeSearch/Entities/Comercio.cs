﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Comercio
    {
        public int Id { get; set; }
        public string nombreComercio { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public string contrasenna { get; set; }
        public TipoComercio tipoComercio { get; set; }
        public List<Imagen> imagenComercios { get; set; }
        public string facebook { get; set; }
        public string instagram { get; set; }
        public string direccion { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public  List<HorarioAtencion> horario { get; set; }
        public List<Promocion> promocion { get; set; }
        public List<Item> articulo { get; set; }
        public List<Comentario> comentario { get; set; }
        public Usuario usuario { get; set; }
        public Comercio()
        {
            usuario = new Usuario();
        }
    }
}