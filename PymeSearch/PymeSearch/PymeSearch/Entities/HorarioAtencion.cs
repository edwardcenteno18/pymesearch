﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class HorarioAtencion
    {
        public int id { get; set; }
        public string dia { get; set; }
        public string inicio { get; set; }
        public string fin { get; set; }
    }
}